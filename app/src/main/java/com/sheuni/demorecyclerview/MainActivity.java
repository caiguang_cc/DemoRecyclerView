package com.sheuni.demorecyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);

        String[] datas = new String[] {
                "list-item",
                "list-item",
                "list-item",
                "list-item",
                "list-item"
        };

        List<String> data = new ArrayList<>(Arrays.asList(datas));

        NoteRecyclerViewAdapter adapter = new NoteRecyclerViewAdapter(data);
        adapter.setItemOnClickListener(new NoteRecyclerViewAdapter.ItemOnClickListener() {
            @Override
            public void onClick(View view, int id, int position) {
                Toast.makeText(MainActivity.this, "你点击了" + position, Toast.LENGTH_LONG).show();
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ItemTouchCallback callback = new ItemTouchCallback(adapter);

        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);
        recyclerView.setAdapter(adapter);

    }
}

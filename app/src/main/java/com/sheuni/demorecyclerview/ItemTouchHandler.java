package com.sheuni.demorecyclerview;

public interface ItemTouchHandler {

    boolean onItemMove(int fromPosition, int toPosition);

    void onItemRemove(int position);
}
